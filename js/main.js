$(document).ready(function() {

	$('#paso-1 .iniciar img').on('click', function(){
		$('#paso-1').removeClass('active');
		$('#paso-2').addClass('active');
		$('.main').addClass('blue');

		ga('send','event','open-app-snoopy','home','siguiente_home');
	})
	$('#paso-2 .iniciar img').on('click', function(){
		$('#paso-2').removeClass('active');
		$('#paso-3').addClass('active');

		ga('send','event','open-app-snoopy','instrucciones','siguiente_instrucciones');
	})
	
	// boton 
	$('#paso-3 .btn-send img').on('click', function(){

		if($("#commentForm").valid() == true){
			console.log('holi');
			$('#paso-3').removeClass('active');
			$('#paso-4').addClass('active');
		}

		ga('send','event','open-app-snoopy','formulario','siguiente_formulario');

		
	})


	$('ul#background li img').on('click', function(){

		$('ul#background li img').removeClass("on");
		$(this).addClass("on");
		// var numb = $(this).attr('data');

	});

	$('#generated').on('click', function(){


		ga('send','event','open-app-snoopy','seleccion-sueno','siguiente_sueno');


		var bg = $('ul#background li img.on').attr('data');

		if($('ul#background li img').hasClass('on')){
		

			var frase = $('#frase').val()

			if(frase == null || frase.length < 4 || frase.length > 36 || /^\s+$/.test(frase)) {
				return false;
			}else {

				name = $('#name').val();
				apellido = $('#apellido').val();
				dni = $('#dni').val();
				phone = $('#celular').val();
				mail = $('#email').val();

				$.post("generated.php", {name: name, apellido: apellido, dni: dni, phone: phone, mail: mail, frase: frase, bg: bg}, function(data){
					
					//console.log(data)

					var data = $.parseJSON(data);

					$(".result").html('<img src="images-generated/'+data.img+'" alt="">');

					$("#download").attr("data", data.img);
					//console.log(data);

					$('#paso-4').removeClass('active');
					$('#paso-5').addClass('active');

				})
			}
		}else {
			return false;
		}	

	});

	$('#download').on('click', function(){

		ga('send','event','open-app-snoopy','final','descargar');

		var img = $(this).attr("data");
		//console.log(img);

		$("#secretIFrame").attr("src","download.php?img="+img);

	})

	$('.share .tw').on('click', function(){
		ga('send','event','open-app-snoopy','final','compartir-twitter');
	})

	$(".share ul li.fb").on("click",function(){
		/*
		FB.ui({
			method: 'feed',
			app_id: '1662961423951641',
			name: 'Sueña en Grande con Open Plaza',
			picture: 'http://circusint.com/2015/open/open_navidad/images/suena_en_grande.png',
			link: 'https://www.facebook.com/OpenPlazaPeru/app_1662961423951641',
			description: 'Ingresa, crea tu foto de portada con los personajes de la película de Snoopy y Charlie Brown y participa del sorteo de merchandising oficial.'
		}, function(response){

			if (response && response.post_id) {
				//alert('Post was published.');
				ga('send','event','open-app-snoopy','final','compartir-facebook');
			}else{
				//alert('Post was not published.');
			}

		});
		*/
		
		if (typeof(FB) != 'undefined' && FB != null ) {

			FB.ui({
		    method: 'share_open_graph',
		    action_type: 'og.shares',
		    action_properties: JSON.stringify({
		        object : {
		           'og:url': 'https://www.facebook.com/OpenPlazaPeru/app_1662961423951641', // your url to share
		           'og:title': 'Sueña en Grande con Open Plaza',
		           'og:description': 'Ingresa, crea tu foto de portada con los personajes de la película de Snoopy y Charlie Brown y participa del sorteo de merchandising oficial.',
		           'og:image': 'http://circusint.com/2015/open/open_navidad/images/suena_en_grande.png'
		        }
		    })
		    },
		    // callback
		    function(response) {
			    if (response && !response.error_message) {
			        // then get post content
			        //alert('successfully posted. Status id : '+response.post_id);
			    } else {
			        //alert('Something went error.');
			    }
			});
		}else {
			//alert('Error');
		}


	})




});